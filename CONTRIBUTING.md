# CONTRIBUTING

Thank you for your interest in helping create and maintain RedundantFileRemover, a duplicate file remover for *NIX systems written in C.

There aren't many requirements for contributing, but please keep in mind that there are some that you need to adhere to, found below.

## Programming Style

The indentation style used in this project is the "[Allman](https://en.wikipedia.org/wiki/Indentation_style#Allman_style)" style, do not mix any other styles in.

Functions and variables are named in camelCase, so don't use any of that snake_case nonsense or whatever else.

Functions, if statements, etc have brackets next to them without spaces.

Inside brackets, there are spaces between the arguments, but not between the arguments and brackets themselves.

## Issues

Please use the issue template in the ISSUES.md file.

## Pull/Merge Requests

Please use the pull request template in the PULLREQUESTS.md file.
