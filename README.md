# RedundantFileRemover

### Latest release is v0.3.1

### Development version is v0.4-DEV

---

Copyright (c) 2021 PCITechie

Duplicate file detector and remover for *nix operating systems (GNU/Linux and FreeBSD officially supported, may not work on others). Licensed under the GPLv3 or, optionally, any later version. See COPYING file or https://www.gnu.org/licenses/gpl-3.0.txt.

## Installation Prerequisites

-As of v0.3, a 64-bit processor is required by RedundantFileRemover.

-As of v0.3.1, Nettle is required by RedundantFileRemover (for MD5 hashing).

-If you want to compile the latest development version, a modern C compiler (GCC and Clang tested) and Git are also required.

## Installing

Binaries for GNU/Linux x86-64 systems can be found at https://gitlab.com/PCITechie/RedundantFileRemover/-/releases/ under "Packages".

To install, simply rename the binary downloaded to `redundantfileremover` and move the program into your binaries directory (usually `/usr/bin`) as root (e.g. `sudo mv -v redundantfileremover /usr/bin`). Reload your terminal and now you can simply run `redundantfileremover` in your terminal from anywhere. To uninstall simply delete this file from your binaries directory as root (e.g. `sudo rm -r /usr/bin/redundantfileremover`).

For all other platforms and architectures, or if the precompiled binaries don't work on your system, see the "Compiling" section.

## Compiling

To compile RedundantFileRemover, go to https://gitlab.com/PCITechie/RedundantFileRemover/-/releases/ and grab the zip file for the version you want to compile.

Alternatively, to download the latest development version, run `git clone https://gitlab.com/PCITechie/RedundantFileRemover`

Unzip the file you have downloaded (if you didnt run `git clone`) and cd into the folder which you either cloned or unzipped.

Run `gcc RedundantFileRemover.c -o redundantfileremover -lnettle` to compile the program.

Now you should have a compiled program, you are able to run it now using `./redundantfileremover`.

To finish installation, move the compiled program into your binaries directory (usually `/usr/bin`) as root (e.g. `sudo mv -v redundantfileremover /usr/bin`). Reload your terminal and now you can simply run `redundantfileremover` in your terminal from anywhere. To uninstall simply delete this file from your binaries directory as root (e.g. `sudo rm -r /usr/bin/redundantfileremover`).

## Warranty Disclaimer

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
