// Copyright (c) 2021 PCITechie
// THIS PROGRAM IS LICENSED UNDER THE GNU GPLv3 OR OPTIONALLY ANY LATER VERSION
// YOU SHOULD HAVE RECIEVED A COPY OF THE LICENSE WITH THIS PROGRAM
// IF YOU DID NOT, A COPY CAN BE FOUND AT https://www.gnu.org/licenses/gpl-3.0.txt.

#define ERR_NOT_DIRECTORY -1
#define ERR_TEMP_FILE_NULL -2
#define ERR_FILE_NULL -3
#define ERR_TOO_MANY_ARGS -4
#define ERR_NOT_ENOUGH_ARGS -5
#define CANCELLED -6

#define SUCCESS 0
#define FILES_DIFFER 1
#define FILES_MATCH 2

#define VERSION "v0.4-DEV"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <signal.h>
#include <sys/stat.h>
#include <nettle/md5.h>

volatile int cancel = 0; // Used to stop what the program is doing when Ctrl+C is pressed

int compareFiles(char *path1, char *path2)
{
	int64_t fileSize1;
	int64_t fileSize2;
	int bytes;
	char buffer[1024];
	char hash1[MD5_DIGEST_SIZE + 2];
	char hash2[MD5_DIGEST_SIZE + 2];
	char char1;
	char char2;
	struct stat statLocation;
	struct md5_ctx md5Context;
	FILE *filePointer1;
	FILE *filePointer2;

	filePointer1 = fopen(path1, "r"); // Open file in read-mode
	filePointer2 = fopen(path2, "r");

	if(filePointer1 == NULL || filePointer2 == NULL)
	{
		fclose(filePointer1);
		fclose(filePointer2);

		return ERR_FILE_NULL;
	}

	if(stat(path1, &statLocation) == 0)
	{
		fileSize1 = statLocation.st_size;
	}
	else
	{
		fclose(filePointer1);
		fclose(filePointer2);

		return ERR_FILE_NULL;
	}

	if(stat(path2, &statLocation) == 0)
	{
		fileSize2 = statLocation.st_size;
	}
	else
	{
		fclose(filePointer1);
		fclose(filePointer2);

		return ERR_FILE_NULL;
	}

	if(fileSize1 != fileSize2)
	{
		fclose(filePointer1);
		fclose(filePointer2);

		return FILES_DIFFER;
	}

	if(fileSize1 < 4096 || fileSize2 < 4096)
	{
		for(int i = 0; i < fileSize1 || i < fileSize2; i++) // This loop reads the first bytes of the files and compares each of them
		{
			char1 = getc(filePointer1);
			char2 = getc(filePointer2);

			if(char1 != char2)
			{
				fclose(filePointer1);
				fclose(filePointer2);

				return FILES_DIFFER;
			}
		}
	}
	else
	{
		for(int i = 0; i < 4096; i++) // This loop reads the first 4 KiB of the files and compares each of them
		{
			char1 = getc(filePointer1);
			char2 = getc(filePointer2);

			if(char1 != char2)
			{
				fclose(filePointer1);
				fclose(filePointer2);

				return FILES_DIFFER;
			}
		}
	}

	fclose(filePointer1);
	fclose(filePointer2);
	filePointer1 = fopen(path1, "r"); // Open file in read-mode
	filePointer2 = fopen(path2, "r");

	md5_init(&md5Context); // These lines generate the MD5 hash for 1st file

	while((bytes = fread(buffer, 1, 1024, filePointer1)) != 0)
	{
		md5_update(&md5Context, bytes, buffer);
	}

	md5_digest(&md5Context, MD5_DIGEST_SIZE, hash1);

	md5_init(&md5Context); // These lines generate the MD5 hash for 2nd file

	while((bytes = fread(buffer, 1, 1024, filePointer2)) != 0)
	{
		md5_update(&md5Context, bytes, buffer);
	}

	md5_digest(&md5Context, MD5_DIGEST_SIZE, hash2);

	fclose(filePointer1);
	fclose(filePointer2);

	hash1[MD5_DIGEST_SIZE + 1] = '\0';
	hash2[MD5_DIGEST_SIZE + 1] = '\0';

	if(strcmp(hash1, hash2) != 0)
	{
		return FILES_DIFFER;
	}

	return FILES_MATCH;
}

void createDirListing(char *dirPath, char recursive) // Completely rewrite this at some point to make the program not rely on external commands
{
	char findCommand[10042] = {0};

	if(recursive == 'Y' || recursive == 'y')
	{
		strcpy(findCommand, "find \""); // These 3 lines turn the user input into a command (find "/example/directory" -print > /tmp/rfrList)
		strcat(findCommand, dirPath);
		strcat(findCommand, "\" -print > /tmp/rfrList");
	}
	else
	{
		strcpy(findCommand, "find \""); // These 3 lines turn the user input into a command (find "/example/directory" -maxdepth 1 -print > /tmp/rfrList)
		strcat(findCommand, dirPath);
		strcat(findCommand, "\" -maxdepth 1 -print > /tmp/rfrList");
	}

	system(findCommand); // This line executes above command via the shell
}

void cancelScan()
{
	cancel = 1;
}

int createDuplicateList()
{
	int lines = 0;
	int dupLines = 0;
	int line1 = 0;
	int line2 = 0;
	int column = 0;
	char character;
	char path1[15000] = {0};
	char path2[15000] = {0};
	char readlinkBuffer[15001];
	struct stat statLocation;
	FILE *filePointer1;
	FILE *filePointer2;

	signal(SIGINT, cancelScan); // When Ctrl+C is pressed, set cancel to 1

	filePointer1 = fopen("/tmp/rfrList", "r");
	filePointer2 = fopen("/tmp/rfrDupList", "w");

	if(filePointer1 == NULL)
	{
		return ERR_TEMP_FILE_NULL;
	}

	while(character != EOF)
	{
		character = getc(filePointer1);

		if(character == '\n')
		{
			lines++;
		}
	}

	for(int i = 0; i <= lines; i++) // While i is less than or equal to the number of lines, run the below code and then increase i by 1
	{
		fclose(filePointer1);
		filePointer1 = fopen("/tmp/rfrList", "r");

		for(int j = 0; j < i; j++)
		{
			fgets(path1, 15000, filePointer1);
		}

		for(int j = 0; j <= strlen(path1); j++)
		{
			if(path1[j] == '\n')
			{
				path1[j] = '\0';
				break;
			}
		}

		for(int j = 0; j <= lines; j++)
		{
			if(cancel == 1)
			{
				cancel = 0;

				return CANCELLED;
			}

			if(i + j + 1 > lines)
			{
				break;
			}

			fclose(filePointer1);
			filePointer1 = fopen("/tmp/rfrList", "r");

			for(int k = 0; k < j + i + 1; k++)
			{
				fgets(path2, 15000, filePointer1);
			}

			for(int k = 0; k <= strlen(path2); k++)
			{
				if(path2[k] == '\n')
				{
					path2[k] = '\0';
				}
			}

			printf("\n\nCOMPARING\n");
			printf("\n%s\n%s", path1, path2);

			if(stat(path1, &statLocation) == 0 && S_ISREG(statLocation.st_mode)) // If path1 is a regular file
			{
				if(stat(path2, &statLocation) == 0 && S_ISREG(statLocation.st_mode)) // If path2 is a regular file
				{
					if((readlink(path1, readlinkBuffer, sizeof(readlinkBuffer) - 1)) != -1) // If path1 is a symbolic link (lstat broke half of the detection for some reason which is why this odd solution is here)
					{
						printf("\n\n%s is a symbolic link, ignoring.", path1); // Ignore the file
					}
					else
					{
						if((readlink(path2, readlinkBuffer, sizeof(readlinkBuffer) - 1)) != -1) // If path2 is a symbolic link
						{
							printf("\n\n%s is a symbolic link, ignoring.", path2); // Ignore the file
						}
						else
						{
							if(compareFiles(path1, path2) == FILES_MATCH && strcmp(path1, path2) != 0) // If both files are equal and paths don't match
							{
								fprintf(filePointer2, strcat(path1, "\n"));
								fprintf(filePointer2, strcat(path2, "\n"));
								dupLines++;

								printf("\n\nFiles match.");
							}
							else if(strcmp(path1, path2) != 0)
							{
								printf("\n\nFiles differ.");
							}
						}
					}
				}
				else // If path2 is not a regular file
				{
					printf("\n\n%s is not a regular file, ignoring.", path2); // Ignore the file
				}
			}
			else // If path1 is not a regular file
			{
				printf("\n\n%s is not a regular file, ignoring.", path1); // Ignore the file
				break; // Break from the for loop
			}
		}
	}

	fclose(filePointer1);
	fclose(filePointer2);
	remove("/tmp/rfrList");

	return dupLines;
}

int deleteDuplicates(int dupLines)
{
	FILE *filePointer;
	char buffer[15000] = {0};
	char deletePath[15000] = {0};

	filePointer = fopen("/tmp/rfrDupList", "r");

	if(filePointer == NULL)
	{
		return ERR_TEMP_FILE_NULL;
	}

	for(int i = 0; i < dupLines; i++)
	{
		fgets(deletePath, 15000, filePointer);
		fgets(buffer, 15000, filePointer);

		for(int j = 0; j <= strlen(deletePath); j++)
		{
			if(deletePath[j] == '\n')
			{
				deletePath[j] = '\0';
			}

			remove(deletePath);
		}
	}

	fclose(filePointer);
	remove("/tmp/rfrDupList");

	return SUCCESS;
}

int cliMain()
{
	int buffer;
	int dupLines;
	int delFiles;
	int delDupReturn;
	char recursive = 'n';
	char dirPath[10001] = {0};
	char dirPathTmp[10000] = {0};
	char duplicatePath[15000] = {0};
	struct stat statLocation;
	FILE *filePointer;

	printf("------------------------------------------\nRedundant File Remover %s\n(c) 2021 PCITechie\nLicensed under GPLv3 or any later version\nhttps://www.gnu.org/licenses/gpl-3.0.txt\n------------------------------------------\n", VERSION);
	printf("\nPlease enter the full path of the directory you would like to check for duplicate files (MAX 9999 CHARS): ");
	scanf("%9999[^\t\n]", dirPath);

	if(!(stat(dirPath, &statLocation) == 0 && S_ISDIR(statLocation.st_mode))) // If dirPath is not a directory
	{
		printf("\033[H\033[J");
		printf("%s is not a directory!\n", dirPath);

		return ERR_NOT_DIRECTORY;
	}

	printf("\n");
	scanf("%c", &buffer);
	printf("Would you like to check all subdirectories of this directory? THIS COULD TAKE A WHILE. (y/N): ");
	scanf("%c", &recursive);

	createDirListing(dirPath, recursive);
	dupLines = createDuplicateList();

	if(dupLines == ERR_TEMP_FILE_NULL)
	{
		printf("\033[H\033[J");
		printf("Failed to open temporary file in function createDuplicateList. Exiting.\n\n");

		exit(ERR_TEMP_FILE_NULL);
	}
	else if(dupLines == CANCELLED)
	{
		return CANCELLED;
	}

	printf("\033[H\033[J");
	printf("\n");
	printf("\033[H\033[J");

	if(dupLines == 0)
	{
		printf("No duplicate files found.\n");

		return SUCCESS;
	}
	else
	{
		printf("Duplicates found:\n");
		filePointer = fopen("/tmp/rfrDupList", "r");

		if(filePointer == NULL)
		{
			printf("\033[H\033[J");
			printf("Failed to open temporary file in function cliMain. Exiting.\n\n");

			exit(ERR_TEMP_FILE_NULL);
		}

		for(int i = 0; i < dupLines; i++)
		{
			fgets(duplicatePath, 15000, filePointer);

			for(int i = 0; i <= strlen(duplicatePath); i++)
			{
				if(duplicatePath[i] == '\n')
				{
					duplicatePath[i] = '\0';
				}
			}

			printf("\n%s is a duplicate of ", duplicatePath);
			fgets(duplicatePath, 15000, filePointer);

			for(int i = 0; i <= strlen(duplicatePath); i++)
			{
				if(duplicatePath[i] == '\n')
				{
					duplicatePath[i] = '\0';
				}
			}

			printf("%s", duplicatePath);
		}

		fclose(filePointer);

		printf("\n");
		scanf("%c", &buffer);
		printf("\nWould you like to delete them? (y/N): ");
		scanf("%c", &delFiles);

		if(delFiles == 'y' || delFiles == 'Y')
		{
			delDupReturn = deleteDuplicates(dupLines);

			if(delDupReturn == ERR_TEMP_FILE_NULL)
			{
				printf("\033[H\033[J");
				printf("Failed to open temporary file in function deleteDuplicates. Exiting.\n\n");

				exit(ERR_TEMP_FILE_NULL);
			}

			printf("\n%i files deleted.\n", dupLines);
		}
		else
		{
			printf("\nYou have chosen to not delete any files.\n");
		}
	}

	return SUCCESS;
}

void displayHelp()
{
	printf("-----------------------------------------------\nCommand line arguments for RedundantFileRemover\n-----------------------------------------------\n\n");
	printf("--help		Displays this help screen\n");
	printf("--info		Displays the information screen, containing links to source code, licensing, etc\n");
	printf("--cli		Starts the program in text mode\n");
	printf("\n");
}

void displayInfo()
{
	printf("------------------------------------\nInformation for RedundantFileRemover\n------------------------------------\n\n");
	printf("Copyright (c) 2021 PCITechie.\n\n");
	printf("This program is free software, licensed under the GNU GPLv3 or, optionally, any later version.\n\n");
	printf("You should have recieved a copy of this license with the program,\n");
	printf("if you did not, a copy can be found at https://www.gnu.org/licenses/gpl-3.0.txt.\n\n");
	printf("The source code for this program is located at https://gitlab.com/PCITechie/RedundantFileRemover.\n\n");
	printf("This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;\n");
  printf("without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n");
	printf("\n");
}

int main(int noOfArguments, char *argument[])
{
	int counter;
	int cliReturn;
	char buffer;
	char runAgain = 'y';

	if(noOfArguments < 2)
	{
		printf("\033[H\033[J");
		printf("Command line argument expected. Run \"redundantfileremover --help\" for help. Exiting.\n\n");

		return ERR_NOT_ENOUGH_ARGS;
	}
	else if(noOfArguments > 2)
	{
		printf("\033[H\033[J");
		printf("%i command line arguments specified, 1 expected. Run \"redundantfileremover --help\" for help. Exiting.\n\n", noOfArguments - 1);

		return ERR_TOO_MANY_ARGS;
	}

	if(strcmp(argument[1], "--cli") == 0)
	{
		while(runAgain == 'y' || runAgain == 'Y')
		{
			if(counter == 1)
			{
				scanf("%c", &buffer);
			}
			else
			{
				counter = 1;
			}

			printf("\033[H\033[J");
			cliReturn = cliMain();

			if(cliReturn == ERR_NOT_DIRECTORY)
			{
				printf("\nThe specified directory does not exist.\n");
			}
			else if(cliReturn == CANCELLED)
			{
				printf("\033[H\033[J");
				printf("\nScan cancelled.\n");
			}

			printf("\n");
			scanf("%c", &buffer);
			printf("Would you like to check another directory? (y/N): ");
			scanf("%c", &runAgain);
		}

		printf("\033[H\033[J");
		printf("Thank you for using Redundant File Remover %s. Copyright (c) 2021 PCITechie, licensed under the GPLv3 or optionally any later version.\n\n", VERSION);
	}
	else if(strcmp(argument[1], "--help") == 0)
	{
		printf("\033[H\033[J");
		displayHelp();
	}
	else if(strcmp(argument[1], "--info") == 0)
	{
		printf("\033[H\033[J");
		displayInfo();
	}
	else
	{
		printf("\n");
		printf("\033[H\033[J");
		printf("Invalid command line argument specified, displaying help.\n\n");
		displayHelp();
	}

	return SUCCESS;
}
